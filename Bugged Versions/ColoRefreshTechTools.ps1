<# 
The {NEW} Colorado Tech Tools - Powershell
Created by Jeffrey B Thomson
Contributions by Christien J Jeter


-------------------------------------Change Log------------------------------------
Prehistory:
-Created the script.

10/19/2022: -Christien, Jeff
-Added a change log! 
-Added MECM functionality. -Christien
-Merged Christien's changed into the most recent version of the script. 
    Here's hoping that nothing breaks! -Jeff

11/29/2022: -Jeff
-Restored old *a account password prompt.
-Disabled MECM Tools, as they do not seem to have any effect.
-Changed $variable names in PC_List function.     
#>

$Global:Title = "PC Refresh Tools"
$host.UI.RawUI.WindowTitle = "$Global:Title"
$Global:Header = ""

#Script Main Menu, allows the user to select which task they wish to execute
function MainMenu {

    Clear-Host
    DrawWindowTitle -Title "$Global:Title - Main Menu" -Header "$Global:Header"
    
    Write-Output "1 - RDP Tools"
    Write-Output "2 - Shutdown Remote PCs"
    Write-Output "3 - Reboot Remote PCs"
    Write-Output "4 - Ping Remote Devices`n"
    Write-Output "P - Copy User Files`n"
    Write-Output "N - Open a New PowerShell Instance"
    Write-Output "M - Get Mac Addresses from Remote PCs"
    Write-Output "`nC - Change Saved *a Account Credentials"
    Write-Output "V - Copy Saved *a Account Password to Clipboard`n"
    Write-Output "`nCTRL+C - Exit Script"
    Write-Output "`n`n"

    $prompt = Read-Host -prompt ">>Make a selection"

    switch ($prompt) {
        "1" {$Global:Header = "";RDP_Tools}
        "2" {$Global:Header = "";Shutdown_Remote_PCs}
        "3" {$Global:Header = "";Reboot_Remote_PCs}
        "4" {$Global:Header = "";Ping_PCs}
        "n" {Start-Process powershell -Verb runas}
        "m" {$Global:Header = "";Get_MacAddresses}
        "c" {$Global:Header = "";A_Account}
        "v" {$Global:Header = "";Clipboard_PW}
        "p" {$Global:Header = "";Copy_Profile_Menu}
        #"" {$Global:Header = "";Add_SU_HC_Group}
        #"" {$Global:Header = "";MECM_Menu}
        Default {Clear-Host;MainMenu}
    }
    $Global:Header = "";MainMenu
}

<#function Add_SU_HC_Group {

    PC_List
    Clear-Host

    foreach ($PC in ($Global:PC_List -split "`r`n")) {
        if ($PC -ne "" -or $PC -eq $null) {
            Write-Host "Adding $PC to Health Connect group"
            Add-ADGroupMember -Identity "CO Production Hyperspace (Users)" -Members $PC
            Write-Host
        }
    }
    Read-Host
}
#>

<#function MECM_Menu {
    Clear-Host
    DrawWindowTitle -Title "$Global:Title - MECM Tools" -Header "$Global:Header"

    Write-Output "1 - Data Discovery"
    Write-Output "2 - Hardware Inventory"
    Write-Output "3 - Software Inventory"
    Write-Output "4 - Machine Policy Retrieval"
    Write-Output "`n5 - Run All`n"
    Write-Output "x - Return to Main Menu`n"

    $prompt = Read-Host -prompt ">>Make a selection"

    switch ($prompt) {
        "1" {$Global:Header = ""; MECM_Action -Action 0 -List (PC_List)}
        "2" {$Global:Header = ""; MECM_Action -Action 1 -List (PC_List)}
        "3" {$Global:Header = ""; MECM_Action -Action 2 -List (PC_List)}
        "4" {$Global:Header = ""; MECM_Action -Action 3 -List (PC_List)}
        "5" {$Global:Header = ""; $list = PC_List;
            MECM_Action -Action 1 -List $list -Auto;
            MECM_Action -Action 2 -List $list -Auto -SkipHeader;
            MECM_Action -Action 3 -List $list -Auto -SkipHeader;
            MECM_Action -Action 0 -List $list -SkipHeader;}
        "x" {$Global:Header = ""; MainMenu}
        default {clear-host; MainMenu}
    }
}

function MECM_Action {
    param (
		[int]$Action,
        [string[]]$List,
        [Switch]$Auto,
        [Switch]$SkipHeader
	)

    $fail = $false

    if ($SkipHeader -ne $true) {
        Write-Output "Running Actions...`n`n"
    }

    foreach ($PC in $List)
    {
        if ($fail) {
            break;
        }
        switch ($Action) 
        {
            0 {
                try {
                    Invoke-WmiMethod -ErrorAction Stop -Computername $PC -Namespace root\ccm -Class sms_client -Name TriggerSchedule "{00000000-0000-0000-0000-000000000003}" | Out-Null;
                    Write-Output "Scheduled Discovery cycle on $PC`n"
                }
                catch {
                    Write-Output "Failed to schedule action on $PC`n"
                }
            }
            1 {
                try {
                    Invoke-WmiMethod -ErrorAction Stop -Computername $PC -Namespace root\ccm -Class sms_client -Name TriggerSchedule "{00000000-0000-0000-0000-000000000001}" | Out-Null;
                    Write-Output "Scheduled Hardware Inventory cycle on $PC`n"
                }
                catch {
                    Write-Output "Failed to schedule action on $PC`n"
                }
            }
            2 {
                try {
                    Invoke-WmiMethod -ErrorAction Stop -Computername $PC -Namespace root\ccm -Class sms_client -Name TriggerSchedule "{00000000-0000-0000-0000-000000000002}" | Out-Null;
                    Write-Output "Scheduled Software Inventory cycle on $PC`n"
                }
                catch {
                    Write-Output "Failed to schedule action on $PC`n"
                }
            }
            3 {
                try {
                    Invoke-WmiMethod -ErrorAction Stop -Computername $PC -Namespace root\ccm -Class sms_client -Name TriggerSchedule "{00000000-0000-0000-0000-000000000021}" | Out-Null;
                    Write-Output "Scheduled Machine Policy Retrieval cycle on $PC`n"
                }
                catch {
                    Write-Output "Failed to schedule action on $PC`n"
                }
            }
            default {Write-Output "Invalid action.`n"; $fail = $true;}
        }
    }

    if ($Auto -ne $true) {
        Read-Host -Prompt "`nPress [Enter] to continue..."
        $Global:Header = ""
        MECM_Menu
    }
}
#>

function Get_MacAddresses {

    $list = PC_List
    Clear-Host

    Remove-Item -Path "C:\Users\$Global:TechNUID\OneDrive - Kaiser Permanente\Documents\MacAddresses.csv" -Force

    foreach ($PC in $list) {
        if ($PC -ne "" -or $PC -eq $null) {
            Write-Host "Getting MAC Address from $PC"
            Get-WmiObject -Class Win32_NetworkAdapterConfiguration -Filter "IPEnabled='True'" -ComputerName $PC |
            Select-Object -Property Hostname, MACAddress, Description |
            Export-CSV -Path "C:\Users\$Global:TechNUID\OneDrive - Kaiser Permanente\Documents\MacAddresses.csv" -NoTypeInformation -Append
            Write-Host ""
        }
    }
    Read-Host -Prompt "`nPress [Enter] to continue..."
    $Global:Header = ""
    MainMenu
}

function Copy_Profile_Menu {

    function Single_User_Nuid {
        $host.UI.RawUI.WindowTitle = "$Global:Title - Copy User Files"
        Clear-Host
        Write-Output "$Global:Title - Copy User Files"
        Write-Output "------------------------------`n"
        $Global:NUID = Read-Host -prompt ">>Enter an NUID"
    }

    function Copy_Profile_PC_Names {
        $Global:SourcePC = Read-Host -prompt ">>Enter Source PC Name"
        $Global:DestinationPC = Read-Host -prompt ">>Enter Destination PC Name"
    }

    function Profile_Copy_Precheck {
        Clear-Host
        Write-Output "$Global:Title - Copying User Files"
        Write-Output "------------------------------`n"

        Write-Host "Testing connection, please wait!"
        $pingSource = Test-Connection -ComputerName $Global:SourcePC -Count 2 -Quiet
        $pingDestination = Test-Connection -ComputerName $Global:DestinationPC -Count 2 -Quiet

        Clear-Host

        Write-Output "$Global:Title - Copying User Files"
        Write-Output "------------------------------`n"

        if ($pingSource -and $pingDestination -eq "true") {Copy_Profile}

        else {
            if ($pingSource -eq "false"){Write-Output "Source PC is offline!"}
            if ($pingDestination -eq "false"){Write-Output "Destination PC is offline!"}

            $prompt = Read-Host ">>Continue anyway? [y/N]"
            if ($prompt -eq "y") {Copy_Profile}
            Read-Host -Prompt "`nPress [Enter] to continue..."
        }
        $Global:Header = ""
    }

    function Copy_Profile {
         Write-Output "Copying $Global:NUID's files from $Global:SourcePC to $Global:DestinationPC. `nThis may take several minutes to complete...`n"

        $TestDestination = Test-Path "\\$Global:DestinationPC\C$\Users\$Global:NUID"

        if ($TestDestination -eq "true") {
            Copy-Item "\\$Global:SourcePC\C$\Users\$Global:NUID\Desktop" "\\$Global:SourcePC\C$\Users\$Global:NUID\" -Recurse -Force
            Copy-Item "\\$Global:SourcePC\C$\Users\$Global:NUID\Music" "\\$Global:SourcePC\C$\Users\$Global:NUID\" -Recurse -Force
            Copy-Item "\\$Global:SourcePC\C$\Users\$Global:NUID\Downloads" "\\$Global:SourcePC\C$\Users\$Global:NUID\" -Recurse -Force
            Copy-Item "\\$Global:SourcePC\C$\Users\$Global:NUID\Favorites" "\\$Global:SourcePC\C$\Users\$Global:NUID\" -Recurse -Force
            Copy-Item "\\$Global:SourcePC\C$\Users\$Global:NUID\Videos" "\\$Global:SourcePC\C$\Users\$Global:NUID\" -Recurse -Force
            }

        else {Write-Output "$Global:NUID has not yet logged onto the new PC, files can not be coppied over yet."}
        

        Read-Host -Prompt "`nPress [Enter] to continue..."
        }

    $host.UI.RawUI.WindowTitle = "$Global:Title - Copy User Files"
    Clear-Host
    Write-Output "$Global:Title - Copy User Files"
    Write-Output "------------------------------`n"
    Write-Output "Notice: Profile transfer tool is still under development.`nCheck destination PC files when finished to confirm successful transfer...`n`n"
    Write-Host "1 - Copy a Single User's Files to New PC"
    #Write-Host "2 - Copy Multiple User Profiles to New PCs"
    Write-Host "X - Return to Main Menu"
    Write-Output "`n"

    $prompt = Read-Host -prompt ">>Make a selection"

    switch ($prompt) {
        "1" {Single_User_Nuid
        Copy_Profile_PC_Names
        }
        "2" {}
        "x" {MainMenu}
        Default {Clear-Host
            Copy_Profile_Menu}
    }
    Copy_Profile
}


#Remotely Shuts down PCs
function Shutdown_Remote_PCs {
    
    $list = PC_List
    Clear-Host
    foreach ($PC in ($list)) {
        if ($PC -ne "" -or $PC -eq $null) {
            Write-Host "`nAttempting to shutdown $PC"
            Stop-Computer -ComputerName $PC -Force
        }
    }
    Read-Host -Prompt "Press [Enter] to continue..."
    Clear-Host
    MainMenu
}

#Remotely reboots PCs
function Reboot_Remote_PCs {
    
    $list = PC_List
    Clear-Host
    foreach ($PC in ($list)) {
        if ($PC -ne "" -or $PC -eq $null) {
            Write-Host "`nAttempting to restart $PC"
            Restart-Computer -ComputerName $PC -Force
        }
    }
    Read-Host -Prompt "Press [Enter] to continue..."
    Clear-Host
    $Global:Header = ""
    MainMenu
}

#Submenu for a collection of RDP Tools
function RDP_Tools {
    Clear-Host

    function RDP_Enable {
		$list = PC_List
        Clear-Host
        foreach ($PC in $list) {
            if ($PC -ne "" -or $null -eq $PC) {
                Write-Output "`nEnabling RDP on $PC"
                reg add "\\$PC\HKLM\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fDenyTSConnections /t REG_DWORD /d "0x0" /f
            }
        }
        Read-Host -Prompt "`nPress [Enter] to continue..."
    }

    function RDP_Disable {
		$list = PC_List
        Clear-Host
        foreach ($PC in $list) {
            if ($PC -ne "" -or $null -eq $PC) {
                Write-Output "`nDisabling RDP on $PC"
                reg add "\\$PC\HKLM\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fDenyTSConnections /t REG_DWORD /d "0x1" /f
            }
        }
        Read-Host -Prompt "`nPress [Enter] to continue..."
    }

    function RDP_Connect { 
		$list = PC_List 	
		$list = [string[]]$list

        function RDP_Connection {
            Clear-Host
            Clipboard_PW
			
			$go = $true;
			$i = $list.length - 1;
            while ($go) {
				for ($a = 0; $a -lt 5; $a++) {
					if ($i -lt 0) {
						break
					}
					$PC = $list[$i]
					$i--
					Write-Output "Connecting to RDP on $PC"
					Start-Process "$env:windir\system32\mstsc.exe" -ArgumentList "/w:1440 /h:900 /v:$PC"
				}
				
				if ($i -lt 0) {
					$go = $false;
					break;
				}
				
				$e = $i + 1;
				Write-Host "`nPress [Enter] to load next batch or [X] to return to menu`nThere are $e devices left in the connection list..."
				$Prompt = Read-Host
				if ($Prompt -eq "x") {
					RDP_Tools
				}
				Clear-Host
				Clipboard_PW
            }
            $Prompt = Read-Host -Prompt "`nPress [R] to retry list or [Enter] key to continue"
            if ($Prompt -eq "r")
            {
                RDP_Connection
            }
            $Global:Header = ""
        }
		
        RDP_Connection    
	}
	
	DrawWindowTitle -Title "$Global:Title - RDP Menu" -Header "$Global:Header"
    Write-Output "1 - Enable RDP"
    Write-Output "2 - Disable RDP"
    Write-Output "3 - Connect to Workstation"
    Write-Output "X - Return to main menu"
    Write-Output "`n`n"

    $prompt = Read-Host -prompt ">>Make a selection"

    switch ($prompt) {
        "1" {RDP_Enable}
        "2" {RDP_Disable}
        "3" {RDP_Connect}
        "X" {MainMenu}
        Default {Clear-Host;RDP_Tools}
    }
    RDP_Tools
}

function Ping_PCs {
    $list = PC_List
    Clear-Host
    Write-Host "Pinging PCs, please wait!`n--------------------------`n"

    $ping = New-Object System.Net.NetworkInformation.Ping

    foreach ($PC in $list) {
        if ($PC -ne "" -or $null -eq $PC) {
            #Write-Output "Pinging $PC...`n---------------------------"
            #ping $PC -n 2
            #Write-Host "`n`n"

            $pingDestination = Test-Connection -ComputerName $PC -Count 2 -Quiet
            if ($pingDestination -eq "true") {
                $ipaddress = ($ping.Send($PC).Address)
                $IPa = $ipaddress.IPAddressToString
                Write-Host "$PC is online."
                Write-Host "$IPa`n"
                }
            else {Write-Host "$PC is offline.`n"}
        }
    }
    Read-Host -Prompt "Press [Enter] to Continue"
    MainMenu
}

function DrawWindowTitle
{
	param (
		[string]$Title,
		[string]$Header
	)

    Write-Output "$Title"
    Write-Output "-----------------------------------------------"
	
	if ($global:Header.length -gt 0) {
		Write-Output "$Header"
        Write-Output "-----------------------------------------------`n"
    }
}

function Clipboard_PW {
    if ($global:cred.GetNetworkCredential().password.length -ne 0){
        Set-Clipboard -Value $global:cred.GetNetworkCredential().password
        $global:Header = "Your *a account password has been copied to the clipboard."
    }
}

function PC_List {

	$signature = @'
	[DllImport("user32.dll", CharSet=CharSet.Auto, ExactSpelling=true)] 
	public static extern short GetAsyncKeyState(int virtualKeyCode);
'@

	Add-Type -MemberDefinition $signature -Name Keyboard -namespace Keys

    Clear-Host

    $TestPath = Test-Path -Path ".\OneDrive - Kaiser Permanente\Documents\PCNames_List.txt" -PathType leaf
    if ($TestPath -eq "true") {$Prompt = Read-Host -Prompt "Load previous list of hostnames? [y/N]"}

    if ($prompt -eq "y") {
        $HostnameInput = ""
        $HostnameInput = Get-Content -Path ".\OneDrive - Kaiser Permanente\Documents\PCNames_List.txt"
        $list = $HostnameInput -split ":"
        $b = 0
        $HostnameList = [string[]]::new(0)
        for ($i = 0; $i -lt $list.length; $i++) {
            if ($list[$i].length -gt 0) {
                $b++
                $temp = [string[]]::new($b)
                $temp[$b - 1] = $list[$i]
                for ($a = 0; $a -lt $b - 1; $a++) {
                    $temp[$a] = $HostnameList[$a]
                }
                $HostnameList = $temp
            }
        }
    }

    else{
        Clear-Host

        Remove-Item ".\OneDrive - Kaiser Permanente\Documents\PCNames_List.txt"
        New-Item ".\OneDrive - Kaiser Permanente\Documents\PCNames_List.txt"
        Clear-Host

        Write-Host "Enter PC Hostnames`nPress Shift+Enter to submit list`n--------------------------------"
        
        $HostnameInput = ""
        $WhileLoopBool = $true
        while ($WhileLoopBool) 
        {
            #Variable to store hostnames, adding a colon at the end of the line for later splitting.
            $HostnameInput += (Read-Host) + ":"
            
            #Detect if the shift and enter keys are pressed simultaneously and exits the while loop.
            if (([Keys.Keyboard]::GetAsyncKeyState(16) -lt 0) -and ([Keys.Keyboard]::GetAsyncKeyState(13) -lt 0)) {
                $WhileLoopBool = $false
                $HostnameOutput = $HostnameInput
                $HostnameOutput | Out-File -FilePath ".\OneDrive - Kaiser Permanente\Documents\PCNames_List.txt" -Append
                break
            }
        }
        $list = $HostnameInput -split ":"
        $b = 0
        $HostnameList = [string[]]::new(0)
        for ($i = 0; $i -lt $list.length; $i++) {
            if ($list[$i].length -gt 0) {
                $b++
                $temp = [string[]]::new($b)
                $temp[$b - 1] = $list[$i]
                for ($a = 0; $a -lt $b - 1; $a++) {
                    $temp[$a] = $HostnameList[$a]
                }
                $HostnameList = $temp
            
            }
        }
    }
    clear-host
    return $HostnameList
}

#Gets admin account credentials 
function A_Account {
    
$Global:username = $env:UserName
    if ($Global:username.length -eq 8) {
        Clear-Host
        $domain = "CS\"
        Write-Host "Enter your *a account password for easy RDP Login.`nPress Cancel to skip.`n"
        $Global:cred = Get-Credential -Credential $domain$env:UserName
        return
    }
    else {
        Clear-Host
        return
    }
}

A_Account
MainMenu